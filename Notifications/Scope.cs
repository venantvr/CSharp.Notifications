using System;
using System.Collections.Generic;

namespace Toolbox.Notifications
{
    public class Scope<T> : IDisposable where T : new()
    {
        private static readonly Stack<T> Stack = new Stack<T>();

        public Scope()
        {
            var instance = new T();
            Stack.Push(instance);
        }

        public Scope(params Action<T>[] initializers)
        {
            var instance = new T();

            foreach (var initializer in initializers)
            {
                initializer.Invoke(instance);
            }

            Stack.Push(instance);
        }

        public static T Current => Stack.Peek();

        public void Dispose()
        {
            var item = Stack.Pop();

            var diposable = item as IDisposable;
            diposable?.Dispose(); // null propagation
        }
    }
}