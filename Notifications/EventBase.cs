﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Toolbox.Notifications
{
    public class EventBase
    {
        public EventBase(Guid identity, DateTime utcDateTime, string eventType, string sender, string user, Guid correlationId, List<KeyValuePair<string, object>> openContent)
        {
            Identity = identity;
            UtcDateTime = utcDateTime;
            EventType = eventType;
            Sender = sender;
            User = user;
            CorrelationId = correlationId;
            OpenContent = openContent;
        }

        public EventBase()
        {
            Identity = Guid.NewGuid();
            UtcDateTime = DateTime.UtcNow;
        }

        //public EventBase(EventBase context, List<KeyValuePair<string, object>> openContent = null)
        //{
        //    Identity = context.Identity;
        //    UtcDateTime = context.UtcDateTime;

        //    var eventType = (EventType)GetType().GetCustomAttributes(typeof(EventType), true).FirstOrDefault();
        //    if (eventType != null) EventType = eventType.Type;

        //    var eventAnnotations = GetType().GetCustomAttributes(typeof(EventAnnotation), true).Cast<EventAnnotation>();
        //    Annotations = eventAnnotations.Select(a => a.Annotation).ToList();

        //    Sender = context.Sender;
        //    User = context.User;
        //    CorrelationId = context.CorrelationId;
        //    OpenContent = openContent;
        //}

        public EventBase(List<KeyValuePair<string, object>> openContent)
        {
            var eventBase = Scope<EventBase>.Current;

            Identity = eventBase.Identity;
            UtcDateTime = eventBase.UtcDateTime;

            var type = GetType();

            var eventType = (EventType) type.GetCustomAttributes(typeof (EventType), true).FirstOrDefault();
            if (eventType != null) EventType = eventType.Type;

            var eventAnnotations = type.GetCustomAttributes(typeof (EventAnnotation), true).Cast<EventAnnotation>();
            Annotations = eventAnnotations.Select(a => a.Annotation).ToList();

            Sender = eventBase.Sender;
            User = eventBase.User;
            CorrelationId = eventBase.CorrelationId;
            OpenContent = openContent;
        }

        public Guid Identity { get; set; }
        public DateTime UtcDateTime { get; set; }
        public string EventType { get; set; }
        public List<string> Annotations { get; set; }
        public string Sender { get; set; }
        public string User { get; set; }
        public Guid CorrelationId { get; set; }
        public List<KeyValuePair<string, object>> OpenContent { get; set; }
    }
}