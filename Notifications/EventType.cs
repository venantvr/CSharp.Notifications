using System;

namespace Toolbox.Notifications
{
    [AttributeUsage(AttributeTargets.Class)]
    public class EventType : Attribute
    {
        public EventType(string type)
        {
            Type = type;
        }

        public string Type { get; }
    }
}