﻿using System;
using System.Collections.Generic;
using Toolbox.Notifications;

namespace Toolbox.EventBaseTests.Business
{
    [EventAnnotation(@"Bank account has been withdrawn")]
    [EventType(@"EventBaseTests.Business.WithdrawnAccountNotification")]
    public class WithdrawnAccountNotification : EventBase
    {
        public WithdrawnAccountNotification()
        {
        }

        [Obsolete]
        public WithdrawnAccountNotification(Guid accountUid, decimal amount, Guid identity, DateTime utcDateTime, string eventType, string sender, string user, Guid correlationId, List<KeyValuePair<string, object>> openContent) : base(identity, utcDateTime, eventType, sender, user, correlationId, openContent)
        {
            AccountUid = accountUid;
            Amount = amount;
        }

        public WithdrawnAccountNotification(Guid accountUid, decimal amount, List<KeyValuePair<string, object>> openContent = null) : base(openContent)
        {
            AccountUid = accountUid;
            Amount = amount;
        }

        public Guid AccountUid { get; set; }

        public decimal Amount { get; set; }
    }
}