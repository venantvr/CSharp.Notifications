﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Toolbox.EventBaseTests.Business;
using Toolbox.EventBaseTests.Infrastructure;
using Toolbox.Notifications;

namespace Toolbox.EventBaseTests
{
    [TestClass]
    public class EventBaseTest
    {
        [TestMethod]
        public void TestInstanciation()
        {
            var account = Guid.NewGuid();
            var correlationId = Guid.NewGuid();

            using (new Scope<EventBase>(e => e.CorrelationId = correlationId, e => e.Sender = @"Sender", e => e.User = @"User"))
            {
                var withdrawnAccountNotification = new WithdrawnAccountNotification(account, 10.0M);

                ServiceBus.Send(withdrawnAccountNotification);

                Assert.AreEqual(withdrawnAccountNotification.AccountUid, account);
                Assert.AreEqual(withdrawnAccountNotification.Amount, 10.0M);
                Assert.AreEqual(withdrawnAccountNotification.Sender, @"Sender");
                Assert.AreEqual(withdrawnAccountNotification.User, @"User");
                Assert.AreEqual(withdrawnAccountNotification.Annotations.First(), @"Bank account has been withdrawn");
                Assert.AreEqual(withdrawnAccountNotification.EventType, @"EventBaseTests.Business.WithdrawnAccountNotification");
            }
        }

        [TestMethod]
        public void TestSerialization()
        {
            var account = Guid.NewGuid();
            var correlationId = Guid.NewGuid();

            using (new Scope<EventBase>(e => e.CorrelationId = correlationId, e => e.Sender = @"Sender", e => e.User = @"User"))
            {
                var withdrawnAccountNotification = new WithdrawnAccountNotification(account, 10.0M);

                var serialized = JsonConvert.SerializeObject(withdrawnAccountNotification);

                var deserialized = JsonConvert.DeserializeObject<WithdrawnAccountNotification>(serialized);

                Assert.AreEqual(deserialized.AccountUid, account);
                Assert.AreEqual(deserialized.Amount, 10.0M);
                Assert.AreEqual(deserialized.Sender, @"Sender");
                Assert.AreEqual(deserialized.User, @"User");
                Assert.AreEqual(deserialized.Annotations.First(), @"Bank account has been withdrawn");
                Assert.AreEqual(deserialized.EventType, @"EventBaseTests.Business.WithdrawnAccountNotification");
            }
        }
    }
}